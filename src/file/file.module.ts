import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { FileSchema } from './schemas/file.schema';
import { UserSchema } from '../user/schemas/user.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'File', schema: FileSchema },
            { name: 'User', schema: UserSchema }
        ]),
    ],
    controllers: [FileController],
    providers: [FileService],
})
export class FileModule { }
