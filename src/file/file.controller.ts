import { Roles } from '../auth/decorators/roles.decorator';
import { Controller, Get, Post, Body, UseGuards, Req, HttpCode, HttpStatus, UseInterceptors, UploadedFile, Request } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileService } from './file.service';
import { AuthGuard, PassportModule } from '@nestjs/passport';
import { CreateFileDto } from './dto/create-file.dto';
import {
    ApiCreatedResponse,
    ApiBadRequestResponse,
    ApiNotFoundResponse,
    ApiConflictResponse,
    ApiUnauthorizedResponse,
    ApiOkResponse,
    ApiForbiddenResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { RolesGuard } from '../auth/guards/roles.guard';

@ApiUseTags('File')
@Controller('file')
@UseGuards(RolesGuard)
export class FileController {
    constructor(
        private readonly fileService: FileService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({ description: 'Data recieved' })
    @ApiUnauthorizedResponse({ description: 'Not authorized.' })
    @ApiForbiddenResponse({ description: 'User has not permitted to this api.' })
    findAll() {
        return this.fileService.findAll();
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('file'))
    @ApiUnauthorizedResponse({ description: 'Not authorized.' })
    @ApiForbiddenResponse({ description: 'User has not permitted to this api.' })
    async create(@UploadedFile() file: any, @Request() req) {
        return await this.fileService.create(file, req);
    }
}