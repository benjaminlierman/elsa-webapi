import { IsNotEmpty, MinLength, MaxLength, IsEmail, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateFileDto {

    constructor(createFileResponse: any) {
        this.name = createFileResponse.name;
        this.path = createFileResponse.path;
    }

    @ApiModelProperty({
        example: 'invoice03.pdf',
        description: 'The name of the file',
        format: 'string',
        minLength: 6,
        maxLength: 255,
    })
    @IsNotEmpty()
    @IsString()
    @MinLength(5)
    @MaxLength(255)
    readonly name: string;

    // path
    @ApiModelProperty({
        example: '/filepath/test/',
        description: 'The path of the file',
        format: 'string',
        minLength: 6,
        maxLength: 255,
    })
    @IsNotEmpty()
    @IsString()
    @MinLength(5)
    @MaxLength(255)
    readonly path: string;

    @ApiModelProperty({
        example: '5e46be81814d10ba32e6337e',
        description: 'ref to the owner of the document',
        format: 'ref',
    })
    @IsNotEmpty()
    readonly user: string;
}
