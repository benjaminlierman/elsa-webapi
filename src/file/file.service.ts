import { Request } from 'express';
import { Injectable, BadRequestException, NotFoundException, ConflictException, Req, Res, Next } from '@nestjs/common';
import { File } from './interfaces/file.interface';
import { CreateFileDto } from './dto/create-file.dto';
import * as AWS from 'aws-sdk';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { v4 as uuid } from 'uuid';

const AWS_S3_BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME;
const s3 = new AWS.S3();
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

@Injectable()
export class FileService {
    constructor(
        @InjectModel('File') private readonly fileModel: Model<File>,
    ) { }

    async create(file: any, req: any): Promise<any> {
        const params = {
            Body: file.buffer,
            Bucket: AWS_S3_BUCKET_NAME,
            Key: uuid(),
            ACL: 'public-read',
        };

        try {
            return s3
                .upload(params)
                .promise()
                .then(
                    data => {
                        const createFileDto = new CreateFileDto({
                            name: data.Key,
                            path: data.Location,
                            user: req.user.ref,
                        });

                        this.createFileModel(createFileDto);
                    },
                    err => {
                        throw new BadRequestException(`Failed to upload image file: ${err}`);
                    },
                );
        } catch (error) {
            throw new BadRequestException(`Failed to upload image file: ${error}`);
        }
    }

    async findAll(): Promise<File[]> {
        return this.fileModel.find();
    }

    async createFileModel(createFileDto: CreateFileDto): Promise<File> {
        const file = new this.fileModel(createFileDto);
        return await file.save();
    }
}
