import * as mongoose from 'mongoose';

export const FileSchema = new mongoose.Schema({
    name: {
        type: String,
        minlength: 6,
        maxlength: 255,
        required: [true, 'NAME_IS_BLANK'],
    },
    path: {
        type: String,
        minlength: 6,
        maxlength: 255,
        required: [true, 'PATH_IS_BLANK'],
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    versionKey: false,
    timestamps: true,
});
